package training.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * strateghelper is created by omenra-nout on 08.10.15.
 *
 * @author Sergey Lakhnov
 */
public class Cart {
    private final DoubleProperty cartTime;
    private final IntegerProperty cartNumber;

    public Cart(){
        this(0,0);
    }
    public Cart(int cartNumber, double cartTime){
        this.cartNumber = new SimpleIntegerProperty(cartNumber);
        this.cartTime = new SimpleDoubleProperty(cartTime);
    }

    public double getCartTime() {
        return cartTime.get();
    }

    public DoubleProperty cartTimeProperty() {
        return cartTime;
    }

    public void setCartTime(double cartTime) {
        this.cartTime.set(cartTime);
    }

    public int getCartNumber() {
        return cartNumber.get();
    }

    public IntegerProperty cartNumberProperty() {
        return cartNumber;
    }

    public void setCartNumber(int cartNumber) {
        this.cartNumber.set(cartNumber);
    }
}
