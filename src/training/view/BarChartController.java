package training.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import training.MainApp;

/**
 * strateghelper is created by omenra-nout on 08.10.15.
 *
 * @author Sergey Lakhnov
 */
public class BarChartController {
    @FXML
    private BarChart<Double, Integer> barChart;

    @FXML
    private CategoryAxis xAxis;

    @FXML
    private NumberAxis yAxis;

    private ObservableList <Integer> numbersCart = FXCollections.observableArrayList();

    @FXML
    private void initialize(){

    }


}
