package training;/**
 * strateghelper is created by omenra-nout on 08.10.15.
 *
 * @author Sergey Lakhnov
 */

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import training.model.Cart;

import java.io.IOException;

public class MainApp extends Application {
    private Stage primaryStage;
    private BorderPane rootLayout;

    private ObservableList<Cart> carts = FXCollections.observableArrayList();

    public MainApp () {
        carts.add(new Cart(1,41.01));
        carts.add(new Cart(3,41.12));
        carts.add(new Cart(5,41.91));
        carts.add(new Cart(7,41.89));
        carts.add(new Cart(12,40.99));
        carts.add(new Cart(88,42.01));
        carts.add(new Cart(65,41.55));
        carts.add(new Cart(58,41.47));
        carts.add(new Cart(61,41.12));
        carts.add(new Cart(2,41.36));
        carts.add(new Cart(9,41.11));
        carts.add(new Cart(13,41.03));
        carts.add(new Cart(4,41.66));

    }
     public ObservableList<Cart> getCarts(){
         return carts;
     }



    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Training");

        initRootLayout();

        showAccordion();

    }

    public void initRootLayout(){
        try {
            FXMLLoader loaderRL = new FXMLLoader(MainApp.class.getResource("view/RootLayout.fxml"));
            rootLayout = loaderRL.load();
            Scene scene = new Scene(rootLayout);

            primaryStage.setScene(scene);

            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    public void showAccordion(){
        try {
        FXMLLoader loaderBarChart = new FXMLLoader();
            loaderBarChart.setLocation(MainApp.class.getResource("view/BarChart.fxml"));

        FXMLLoader loaderCartDataOverview = new FXMLLoader();
            loaderCartDataOverview.setLocation(MainApp.class.getResource("view/CartDataOverview.fxml"));
        FXMLLoader loaderTimer = new FXMLLoader();
            loaderTimer.setLocation(MainApp.class.getResource("view/Timer.fxml"));


            TitledPane barChartPane = loaderBarChart.load();
            TitledPane cartDataOverviewPane = loaderCartDataOverview.load();
            TitledPane timerPane = loaderTimer.load();

            Accordion accordion = new Accordion();
            accordion.getPanes().addAll(barChartPane, cartDataOverviewPane, timerPane);

            rootLayout.setCenter(accordion);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Stage getPrimaryStage(){
        return primaryStage;
    }


    public static void main(String[] args) {
        launch(args);
    }
}
