package strateghelper.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by OmenRa on 21.09.15.
 * @author Sergey Lakhnov
 */
public class Team {
    private final IntegerProperty teamNumber;
    private final IntegerProperty teamNumberCart;
    private final IntegerProperty teamPoleposition;
    private final StringProperty teamName;
    private final StringProperty teamPilotsName;
    private final StringProperty teamCity;

/**
 * Default constructor.
 * */

    public Team() {
        this(0, null);

    }



    /**
     * Constructor with some initial data.
     *
     * @param teamNumber
     * @param teamName
     */
    public Team(int teamNumber, String teamName){
        this.teamNumber = new SimpleIntegerProperty(teamNumber);
        this.teamName = new SimpleStringProperty(teamName);

        // todo Some initial dummy data, just for convenient testing.
        this.teamNumberCart = new SimpleIntegerProperty(0);
        this.teamPoleposition = new SimpleIntegerProperty(0);
        this.teamPilotsName = new SimpleStringProperty("++++++++");
        this.teamCity = new SimpleStringProperty("++++++++");
    }

    public Integer getTeamNumber() {
        return teamNumber.get();
    }

    public IntegerProperty teamNumberProperty() {
        return teamNumber;
    }

    public void setTeamNumber(int teamNumber) {
        this.teamNumber.set(teamNumber);
    }

    public int getTeamNumberCart() {
        return teamNumberCart.get();
    }

    public IntegerProperty teamNumberCartProperty() {
        return teamNumberCart;
    }

    public void setTeamNumberCart(int teamNumberCart) {
        this.teamNumberCart.set(teamNumberCart);
    }

    public int getTeamPoleposition() {
        return teamPoleposition.get();
    }

    public IntegerProperty teamPolepositionProperty() {
        return teamPoleposition;
    }

    public void setTeamPoleposition(int teamPoleposition) {
        this.teamPoleposition.set(teamPoleposition);
    }

    public String getTeamName() {
        return teamName.get();
    }

    public StringProperty teamNameProperty() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName.set(teamName);
    }

    public String getTeamPilotsName() {
        return teamPilotsName.get();
    }

    public StringProperty teamPilotsNameProperty() {
        return teamPilotsName;
    }

    public void setTeamPilotsName(String teamPilotsName) {
        this.teamPilotsName.set(teamPilotsName);
    }

    public String getTeamCity() {
        return teamCity.get();
    }

    public StringProperty teamCityProperty() {
        return teamCity;
    }

    public void setTeamCity(String teamCity) {
        this.teamCity.set(teamCity);
    }
}
