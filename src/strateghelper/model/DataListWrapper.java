package strateghelper.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created strateghelper on 07.10.15.
 *
 * @author OmenRa
 * @email Seomaster.LSI@gmail.com
 * --------------------------------
 * Helper class to wrap a list of Teams and Carts . This is used for saving the
 * list of Teams and Carts to XML.
 *  Based on the
 *  @author Marco Jakob
 *  code
 */
@XmlRootElement (name = "data")
public class DataListWrapper {
    private List<Team> teams;
    private List<Cart> carts;


    @XmlElement (name = "teams")
    public List<Team> getTeams(){
        return teams;
    }

    @XmlElement (name = "carts")
    public List<Cart> getCarts(){
        return carts;
    }

    public void setTeams(List<Team> teams){
        this.teams = teams;
    }
    public void setCarts(List<Cart> carts){this.carts = carts;}
}

