package strateghelper.model;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 * Created strateghelper on 22.10.15.
 *
 * @author OmenRa
 * @email Seomaster.LSI@gmail.com
 */
public class MyPlayer {
    private final String sourceMP3 = "etc/mus/zvonki_006.mp3";
    private final String sourceOGG = "etc/mus/alarm_001.ogg";
    private final String sourceWAV = "etc/mus/alarm_004.wav";



    /**
     * empty constructor
     * */
    public MyPlayer() {
    }
    /**
     * constructor
     */

    public String getSourceMP3() {
        return sourceMP3;
    }

    public String getSourceOGG() {
        return sourceOGG;
    }

    public String getSourceWAV() {
        return sourceWAV;
    }

   public  void getSound(){
        System.out.println("MP3 playing now");

    // media  = new Media(getClass().getResource(getSourceMP3()).toExternalForm());
        //MediaPlayer player = new MediaPlayer(new Media(getClass().getResource(getSourceMP3()).toExternalForm()));
        //player.play();

        System.out.println("wav");
        MediaPlayer player  = new MediaPlayer(new Media(getClass().getResource(getSourceWAV()).toExternalForm()));
         player.play();

//       System.out.println("OGG");
//       player = new MediaPlayer(new Media(getClass().getResource(getSourceOGG()).toExternalForm()));
//       player.play();


    }
}
