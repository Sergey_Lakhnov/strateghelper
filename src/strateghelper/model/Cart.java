package strateghelper.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * Created by omenra on 27.09.15.
 * @author Sergey Lakhnov
 */

public class Cart {
    private final DoubleProperty cartTime;
    private final IntegerProperty cartNumber;

    /**
     * default constructor
     */
    public Cart(){
     this(0,0);

    }
    /**
     * Constructor .
     *
     * @param cartNumber
     * @param cartTime
     */
    public Cart(int cartNumber, double cartTime){
        this.cartNumber = new SimpleIntegerProperty(cartNumber);
        this.cartTime = new SimpleDoubleProperty(cartTime);
    }

    public double getCartTime() {
        return cartTime.get();
    }

    public DoubleProperty cartTimeProperty() {
        return cartTime;
    }

    public void setCartTime(double cartTime) {
        this.cartTime.set(cartTime);
    }

    public int getCartNumber() {
        return cartNumber.get();
    }

    public IntegerProperty cartNumberProperty() {
        return cartNumber;
    }

    public void setCartNumber(int cartNumber) {
        this.cartNumber.set(cartNumber);
    }
}
