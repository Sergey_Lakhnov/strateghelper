package strateghelper.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import strateghelper.model.Team;

/**
 * Created by OmenRa on 25.09.15.
 *
 * @author Marco Jacob & Sergey Lakhnov
 *
 */
public class TeamEditDialogController {
    /***/
    @FXML
    private TextField teamNumberField;
    @FXML
    private TextField teamNumberCartField;
    @FXML
    private TextField teamPolepositionField;
    @FXML
    private TextField teamNameField;
    @FXML
    private TextField teamPilotsNameField;
    @FXML
    private TextField teamCityField;

    private Stage dialogStage;
    private Team team;
    private boolean okClicked = false;

    private Alert alert = new Alert(Alert.AlertType.ERROR);



    private String message;
    /**
     * Initializes the controller class. This method is automautically called
     * after the fxml file has been loaded.
     */

    @FXML
    private void initialize(){

    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
   public void setDialogStage (Stage dialogStage){
       this.dialogStage = dialogStage;
   }

    /**
     * Sets the person to be edited in the dialog.
     *
     * @param team
     */
    public void setTeam (Team team){
        this.team = team;

        teamNumberField.setText(Integer.toString(team.getTeamNumber()));
        teamNameField.setText(team.getTeamName());
        teamNumberCartField.setText(Integer.toString(team.getTeamNumberCart()));
        teamPolepositionField.setText(Integer.toString(team.getTeamPoleposition()));
        teamPilotsNameField.setText(team.getTeamName());
        teamCityField.setText(team.getTeamCity());
    }
    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk(){
        if (isInputValid()) {
            team.setTeamCity(teamCityField.getText());
            team.setTeamName(teamNameField.getText());
            team.setTeamNumber(Integer.parseInt(teamNumberField.getText()));
            team.setTeamNumberCart(Integer.parseInt(teamNumberCartField.getText()));
            team.setTeamPilotsName(teamPilotsNameField.getText());
            team.setTeamPoleposition(Integer.parseInt(teamPolepositionField.getText()));

            okClicked = true;
            dialogStage.close();

        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel(){
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid(){
        message ="";

        if (teamCityField.getText() == null || teamCityField.getText().length() == 0){
//            errorMessage += "Город не введен!\n";
            message += "Город не введен\n\r";
        }

        if (teamNameField.getText() == null || teamCityField.getText().length() == 0) {
//            errorMessage += "Нет названия команды!\n";
            message += "Нет названия команды!\n\r";
        }

        if (teamPilotsNameField.getText() == null || teamCityField.getText().length() == 0) {
//            errorMessage += "А кто, собственно, пилоты? Укажите!\n";
            message += "А кто, собственно, пилоты? Укажите!\n\r";
        }

        if (teamNumberCartField.getText() == null || teamNumberCartField.getText().length() == 0) {
//            errorMessage += "Номер квалификационного карта не введен!\n";
            message += "Номер квалификационного карта не введен!\n\r";
        } else {
            // try to parse int
            try {
                Integer.parseInt(teamNumberCartField.getText());
            } catch (NumberFormatException e) {
//                errorMessage += "ВВЕДЕНЫ НЕ ЦИФРЫ!!!\n";
                message += "Номер карта надо вводить цифрами!\n\r";
            }
        }
        if (teamNumberField.getText() == null || teamNumberField.getText().length() == 0) {
//            errorMessage += "Номер команды не введен!\n";
            message += "Номер команды не введен!\n\r";
        } else {
            // try to parse int
            try {
                Integer.parseInt(teamNumberField.getText());
            } catch (NumberFormatException e) {
//                errorMessage += "ВВЕДЕНЫ НЕ ЦИФРЫ!!!\n";
                message += "Номер команды буквами? Попробуте ввести цифры\n\r";
            }
        }
        if (teamPolepositionField.getText() == null || teamPolepositionField.getText().length() == 0) {
//            errorMessage += "Укажите стартовое место!\n";
            message += "Укажите стартовое место!\n\r";
        } else {
            // try to parse int
            try {
                Integer.parseInt(teamPolepositionField.getText());
                if (Integer.parseInt(teamPolepositionField.getText()) < 0) {
                    message += "Отрицательное стартовое место?!!\n\r " +
                            "Необходимо выпить энергетик - налицо усталость!\n\r";
                }
            } catch (NumberFormatException e) {
//                errorMessage += "ВВЕДЕНЫ НЕ ЦИФРЫ!!!\n";
                message += "Стартовое место - тоже цифры!\n\r";
                }
            }
        if (message.length() == 0) {
            return true;
        } else {
        return alertFalse();
        }

    }
/**
 * @return false and output alert windows with errors
 */
    private boolean alertFalse (){
        alert.setTitle("Ошибка заполнения");

        alert.setHeaderText("Пожалуйста, повторите ввод ПРАВИЛЬНО!");
        alert.setContentText(message);
        alert.showAndWait();

        return false;
    }


}


