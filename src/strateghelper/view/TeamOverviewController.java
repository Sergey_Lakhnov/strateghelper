package strateghelper.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
// import org.controlsfx.dialog.Dialogs;
import strateghelper.Main;
import strateghelper.model.Team;
/**
 * Created by OmenRa on 21.09.15.
 * @author Sergey Lakhnov
 */

public class TeamOverviewController {
    @FXML
    private TableView<Team> teamTable;

    @FXML
    private TableColumn<Team, Integer> teamNumberColumn;

    // todo recomendation
//    @FXML
//    private TableColumn<Team, Integer> teamPolepositionColumn;

    @FXML
    private TableColumn<Team, String> teamNameColumn;

    @FXML
    private Label teamNumberLabel;
    @FXML
    private Label teamNumberCartLabel;
    @FXML
    private Label teamPolepositionLabel;
    @FXML
    private Label teamNameLabel;
    @FXML
    private Label teamPilotsNameLabel;
    @FXML
    private Label teamCityLabel;

    // Reference to the main application.
    private Main main;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public TeamOverviewController () {

    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize(){


        // Initialize the person table with the two columns.

      teamNumberColumn.setCellValueFactory(cellData -> cellData.getValue().teamNumberProperty().asObject());
      teamNameColumn.setCellValueFactory(cellData -> cellData.getValue().teamNameProperty());

       //teamPolepositionColumn.setCellValueFactory(cellData -> cellData.getValue().teamPolepositionProperty().asObject());



        // clear team details
        showTeamDetails(null);

        // listen for selection changes and show the teams details when changed
        teamTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showTeamDetails(newValue));
    }





    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param main
     */
    public void setMain (Main main){
        this.main = main;

        // Add observable list data to the table

        teamTable.setItems(main.getTeamData());



    }
    /**
     * Fills all text fields to show details about the person.
     * If the specified person is null, all text fields are cleared.
     *
     * @param team the person or null
     */
    private void showTeamDetails(Team team){
        if (team != null){
            // Fill the labels with info from the team object
            teamNumberLabel.setText(Integer.toString(team.getTeamNumber()));
            teamNumberCartLabel.setText(Integer.toString(team.getTeamNumberCart()));
            teamPolepositionLabel.setText(Integer.toString(team.getTeamPoleposition()));
            teamNameLabel.setText(team.getTeamName());
            teamPilotsNameLabel.setText(team.getTeamPilotsName());
            teamCityLabel.setText(team.getTeamCity());

        }else {
            //person is null, remove the text
            teamNumberLabel.setText("");
            teamNumberCartLabel.setText("");
            teamPolepositionLabel.setText("");
            teamNameLabel.setText("");
            teamPilotsNameLabel.setText("");
            teamCityLabel.setText("");

        }
    }
    /**
     * Called when the user clicks on the delete button.
     */
    @FXML
    private void handleDeleteTeam(){
        int selectedIndex = teamTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            teamTable.getItems().remove(selectedIndex);
        } else {
            // nothing selected
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ничего не выбрано");
            alert.setHeaderText("Команда не выбрана");
            alert.setContentText("Пожалуйста, выберите команду из таблицы!");

            alert.showAndWait();

        }

    }
/**
 * Called when the user clicks the new button. Opens a dialog to edit
 * details for a new person.
 */
    @FXML
    // todo ограничитель!!!
    private void handleNewTeam() {
       Team tempTeam = new Team();
        boolean okClicked = main.showTeamEditDialog(tempTeam);
        if (okClicked){
            main.getTeamData().add(tempTeam);
        }
    }

    /**
     * Called when the user clicks the edit button. Opens a dialog to edit
     * details for the selected person.
     */
    @FXML
    private void handleEditTeam() {
        Team selectedTeam = teamTable.getSelectionModel().getSelectedItem();
        if (selectedTeam != null) {
            boolean okClicked = main.showTeamEditDialog(selectedTeam);
            if (okClicked) {
                showTeamDetails(selectedTeam);
            }
        } else {
            // nothing selected.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка выбора");
            alert.setHeaderText("Ни одна команда не выбрана");
            alert.setContentText("Пожалуйста, сделайте свой выбор!");
            alert.showAndWait();



        }

    }

}

