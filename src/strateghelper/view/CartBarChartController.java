package strateghelper.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import strateghelper.Main;
import strateghelper.model.Cart;

import java.util.ArrayList;

/**
 * Created strateghelper on 06.10.15.
 *
 * @author OmenRa
 * @email Seomaster.LSI@gmail.com
 */
public class CartBarChartController {

    // initialise the time bar chart


    @FXML
    private CategoryAxis xAxis = new CategoryAxis(); // cart axis

    @FXML
    private NumberAxis yAxis = new NumberAxis(25.00, 70.00, 0.01); // time axis

    @FXML
    private BarChart<String, Number> barChartTime = new BarChart<>(xAxis, yAxis);


    private ObservableList<Cart> cartObservableList = FXCollections.observableArrayList();

    private Main main;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Get an array with number of cart
        //todo
        setCartTime( cartObservableList);

    }
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param main
     */
    public void setMain (Main main){
        this.main = main;
    }



    public void setCartTime(ObservableList<Cart> cartObservableList) {
        ArrayList<String> cartAxis = new ArrayList<>() ;
        for (int i = 0; i < cartObservableList.size(); i++) {
            cartAxis.add(i, Integer.toString(cartObservableList.get(i).getCartNumber()));
        }

        XYChart.Series<String,Number> series = new XYChart.Series<>();

        for (int i = 0; i < cartAxis.size() ; i++) {
            series.getData().add(new XYChart.Data<>(cartAxis.get(i), i));

        }
        barChartTime.getData().add(series);
    }



}



