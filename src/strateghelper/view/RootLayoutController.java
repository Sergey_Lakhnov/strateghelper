package strateghelper.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import strateghelper.Main;

import java.io.File;

/**
 * Created strateghelper on 07.10.15.
 *
 * @author OmenRa
 * @email Seomaster.LSI@gmail.com
 *
 * The controller for the root layout. The root layout provides the basic
 * application layout containing a menu bar and space where other JavaFX
 * elements can be placed.
 *
 * @author Marco Jakob
 */
public class RootLayoutController {
    // reference to the main application

    private Main main;

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param main
     */
    public void setMain (Main main){
        this.main = main;
    }

    /**
     * Creates an empty address book of teams.
     */
    @FXML
    private void handleNew () {
        main.getTeamData().clear();
        main.setTeamFilePath(null);
    }

    /**
     * Opens a FileChooser to let the user select an address book to load.
     */
    @FXML
    private void handleOpen(){
        FileChooser fileChooser = new FileChooser();

        //set extension filter
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("XML файлы (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extensionFilter);

        //Show save file dialog
        File file = fileChooser.showOpenDialog(main.getPrimaryStage());

        if (file != null){
            main.loadTeamDataFromFile(file);
        }

    }
    /**
     * Saves the file to the team file that is currently open. If there is no
     * open file, the "save as" dialog is shown.
     */
    @FXML
    private void handleSave(){
        File teamFile = main.getTeamFilePath();
        if (teamFile != null){
            main.saveTeamDataToFile(teamFile);
        } else {
            handleSaveAs();
        }
    }
    /**
     * Opens a FileChooser to let the user select a file to save to.
     */
    @FXML
    private void handleSaveAs(){
        FileChooser fileChooser = new FileChooser();

        //set extension filter
        FileChooser.ExtensionFilter exFilter =new FileChooser.ExtensionFilter("XML файлы (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(exFilter);

        //Show save file dialog
        File file = fileChooser.showSaveDialog(main.getPrimaryStage());

        if (file != null){
            // Make sure it has the correct extension
            if (!file.getPath().endsWith(".xml")){
                file = new File(file.getPath() + ".xml");
            }
            main.saveTeamDataToFile(file);
        }
    }

    /**
     * Opens an about dialog.
     */
    //todo barchart with msc fkn ndstrl (окошко с эквалайзером)
    @FXML
    private void handleAbout(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Информация");
        alert.setHeaderText(null);
       alert.setResizable(true);
        alert.setContentText("Большое спасибо Marco Jakob (http://code.makery.ch)\n - за его замечательные уроки и" +
                " Andrew Bystrov\n(http://ru.stackoverflow.com/users/177093/andrew-bystrov)\nза толчок в верном направлении\n" +
                "Seomaster.LSI@gmail.com");
        alert.showAndWait();
    }
    @FXML
    private void handleExit(){
        System.exit(0);
    }

}
