package strateghelper.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import strateghelper.model.Cart;

/**
 * Created strateghelper on 14.10.15.
 *
 * @author OmenRa
 * @email Seomaster.LSI@gmail.com
 */
public class CartEditController {
    @FXML
    private TextField cartNumberField;
    @FXML
    private TextField cartTimeField;

    private Stage dialogStage;
    private Cart cart;
    private boolean okClicked = false;

    private Alert alert = new Alert(Alert.AlertType.ERROR);

    private String message;

    @FXML
    private void initialize(){}

    public void setCart (Cart cart){
        this.cart = cart;

        cartNumberField.setText(Integer.toString(cart.getCartNumber()));
        cartTimeField.setText(Double.toString(cart.getCartTime()));
    }
}
