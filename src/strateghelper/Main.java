package strateghelper;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import strateghelper.model.Cart;
import strateghelper.model.DataListWrapper;
import strateghelper.model.MyPlayer;
import strateghelper.model.Team;
import strateghelper.view.CartBarChartController;
import strateghelper.view.RootLayoutController;
import strateghelper.view.TeamEditDialogController;
import strateghelper.view.TeamOverviewController;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.prefs.Preferences;

public class Main extends Application {

    private String version = "Strateg helper W15_10-0.2 b";

    private Stage primaryStage;

    private BorderPane rootLayout;



    /**
     * The data as an observable list of Teams.
     */
    private ObservableList<Team> teamData = FXCollections.observableArrayList();

    /**
     * The data as an observable list of Carts
     */
    private ObservableList<Cart> cartData = FXCollections.observableArrayList();
    /**
     * Constructor
     */
    public Main (){
        // todo
        //Ad some sample Team data

        teamData.add(new Team(1,"First"));


        // todo
        // ad some sample Cart Data

        cartData.add(new Cart(1,39.04));





    }
    /**
     * Returns the data as an observable list of Teams.
     * @return
     */
    public ObservableList<Team> getTeamData() {

        return teamData;
    }
    /**
     * Returns the data as an Observable List of Carts
     * @return
     */
    public ObservableList<Cart> getCartData(){
        return cartData;
    }



    @Override
    public void start (Stage primaryStage) {

        this.primaryStage = primaryStage;
        this.primaryStage.setTitle(version);
        this.primaryStage.getIcons().add(new Image("strateghelper/etc/pic/Big_Win-32.png"));

        initRootLayout();

        showAccordion();

    }

        /**
         * Initializes the root layout.
         */

    public void initRootLayout() {

       try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/RootLayout.fxml"));

            rootLayout = loader.load();
           Scene scene = new Scene(rootLayout);

           // Show the scene containing the root layout.
            primaryStage.setScene(scene);

           // Give the controller acces to the main app
           RootLayoutController controller = loader.getController();
           controller.setMain(this);


            primaryStage.show();




        } catch (IOException e) {
            e.printStackTrace();
        }

        /**
         * experimental sound blocks
         */
        Alert alSound = new Alert(Alert.AlertType.INFORMATION);
        alSound.setHeaderText("MUSIC");
        alSound.setContentText("playing");
        MyPlayer player = new MyPlayer();
        alSound.showAndWait();
        /*player.getSound();*/

        // try to load last opened file
        File file = getTeamFilePath();
        if (file != null){
            loadTeamDataFromFile(file);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(version);
            alert.setHeaderText("Предыдущие данные успешно загружены");
            alert.setContentText("файл:\n" + file.getName() + "\nместо сохранения:\n" + file.getAbsolutePath());
            alert.showAndWait();

        }else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Нет данных");
            alert.setHeaderText("Отсутствуют данные");
            alert.setContentText("Похоже, что нет сохраненных данных.\nНадо заполнить!");
            alert.showAndWait();



        }
    }
    /**
     * Shows the cart bar charts
     *
     * Shows the person overview
     */
    public void showAccordion() {

        try {
            // Load team overview.
            FXMLLoader loaderTeamOverview = new FXMLLoader();
            loaderTeamOverview.setLocation(Main.class.getResource("view/TeamOverview.fxml"));

            // Load CartOverview
            FXMLLoader loaderCartOverview = new FXMLLoader();
            loaderCartOverview.setLocation(Main.class.getResource("view/CartOverview.fxml"));


            TitledPane teamOverviewTitledPane = loaderTeamOverview.load();
            TitledPane cartOverviewTitledPane = loaderCartOverview.load() ;


            // Add Titled Pane to Accordion
            Accordion accordion = new Accordion();

            accordion.getPanes().addAll(teamOverviewTitledPane,cartOverviewTitledPane);

            // Set accordion to Center

            rootLayout.setCenter(accordion);

            //Give the controller acces to the main app.

            TeamOverviewController teamOverviewController = loaderTeamOverview.getController();
            CartBarChartController cartBarChartController = loaderCartOverview.getController();

            teamOverviewController.setMain(this);
            cartBarChartController.setMain(this);


        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    /**
     * Returns the main stage.
     *
     * @return
     */

    public Stage getPrimaryStage() {
        return primaryStage;
    }



    public static void main(String[] args) {

        launch(args);
    }
    /**
     * Opens a dialog, to edit details of some cart. If the user
     * clicks OK, the changes are saved into the provided cart object and true
     * is returned.
     *
     * @param cart the cart object to be edited
     * @return true if the user clicked OK, false otherwise.
     * */
    public boolean showCartEditDialog (Cart cart){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/CartEditDialog.fxml"));
            AnchorPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("редактирование карта");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }


        return false;
    }
    /**
     * Opens a dialog to edit details for the specified team. If the user
     * clicks OK, the changes are saved into the provided team object and true
     * is returned.
     *
     * @param team the team object to be edited
     * @return true if the user clicked OK, false otherwise.
     */
    public boolean showTeamEditDialog(Team team){
        try{
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/TeamEditDialog.fxml"));
            AnchorPane page = loader.load();

            // create the dialogStage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Редактирование команды");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.

            TeamEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setTeam(team);

            // Show the dialog and wait until the user closes it

            dialogStage.showAndWait();

            return controller.isOkClicked();



        } catch (IOException e){
            e.printStackTrace();
            return false;
        }
    }
    /**
     * Returns the team file preference, i.e. the file that was last opened.
     * The preference is read from the OS specific registry. If no such
     * preference can be found, null is returned.
     *
     * @return
     */
    public File getTeamFilePath () {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        String filePath = prefs.get("filePath",null);

        if (filePath != null) {
            return new File(filePath);
        } else {
            return null;
        }
    }

    /**
     * Sets the file path of the currently loaded file. The path is persisted in
     * the OS specific registry.
     *
     * @param file the file or null to remove the path
     */
    public void setTeamFilePath (File file){
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        if (file != null){
            prefs.put("filePath", file.getPath());

            // Update the stage title
            primaryStage.setTitle(version + " - " + file.getName());
        } else {
            prefs.remove("filePath");
            //Update the stage title
            primaryStage.setTitle(version + " - " + "нет сохранённых данных");
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("НЕТ СОХРАНЕННЫХ ДАННЫХ!");
            alert.setHeaderText(null);
            alert.setContentText("Нет данных для загрузки. Операция отменена.");
            alert.showAndWait();
        }
    }

/**
 * Loads team data from the specified file. The current team data will
 * be replaced.
 *
 * @param file
 */
    public void loadTeamDataFromFile(File file){
        try {
            JAXBContext contextTeam = JAXBContext.newInstance(DataListWrapper.class);
            Unmarshaller umTeam = contextTeam.createUnmarshaller();




            //Reading XML from the file and unmarshalling
            DataListWrapper wrapperTeam = (DataListWrapper) umTeam.unmarshal(file);

            teamData.clear();
            teamData.addAll(wrapperTeam.getTeams());
            cartData.addAll(wrapperTeam.getCarts());



            setTeamFilePath(file);
        } catch (JAXBException e) { // catch ANY exceptions
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setResizable(true);
            alert.setTitle("НО ЭТО НЕ СТРАШНО");
            alert.setHeaderText("Не нашел в реестре пути прошлого\nсохранения :(");
            alert.setContentText("Не смог загрузить\nданные по пути:\n" + file.getPath());

            //Exception ex = e;

            //create expanded Exeption
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String exceptionText = sw.toString();

            Label label = new Label("Содержание ошибки:");

            TextArea textArea = new TextArea(exceptionText);
            textArea.setEditable(false);
            textArea.setWrapText(true);

            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(textArea, Priority.ALWAYS);
            GridPane.setHgrow(textArea, Priority.ALWAYS);

            GridPane expContent = new GridPane();
            expContent.setMaxWidth(Double.MAX_VALUE);
            expContent.add(label, 0, 0);
            expContent.add(textArea, 0, 1);

            // Set expandable Exception into the dialog pane
            alert.getDialogPane().setExpandableContent(expContent);

            alert.showAndWait();
            // e.printStackTrace();
        }
    }
    /**
     * Saves the current team data to the specified file.
     *
     * @param file
     */
    public void saveTeamDataToFile(File file){
        file.setWritable(true);
        try {
            JAXBContext contextTeam = JAXBContext.newInstance(DataListWrapper.class);
            Marshaller mTeam = contextTeam.createMarshaller();
            mTeam.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);



            // Wrapping Team data
            DataListWrapper wrapper = new DataListWrapper();
            wrapper.setTeams(teamData);
            wrapper.setCarts(cartData);



            // Marshalling and saving XML to file
            mTeam.marshal(wrapper, file);


            // Save the file path to the registry.
            setTeamFilePath(file);

            file.setWritable(false);
        } catch (JAXBException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR!");
            alert.setHeaderText("ОШИБКА СОХРАНЕНИЯ!");
            alert.setContentText("Не смог сохранить\nданные по пути:\n" + file.getPath());

            //Exception ex = e;

            //create expanded Exeption
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String exceptionText = sw.toString();

            Label label = new Label("Содержание ошибки:");

            TextArea textArea = new TextArea(exceptionText);
            textArea.setEditable(false);
            textArea.setWrapText(true);

            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(textArea, Priority.ALWAYS);
            GridPane.setHgrow(textArea, Priority.ALWAYS);

            GridPane expContent = new GridPane();
            expContent.setMaxWidth(Double.MAX_VALUE);
            expContent.add(label, 0, 0);
            expContent.add(textArea, 0, 1);

            // Set expandable Exception into the dialog pane
            alert.getDialogPane().setExpandableContent(expContent);

            alert.showAndWait();
        }


    }
    public void showBarChart (){}
}
